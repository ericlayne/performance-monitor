extends Panel

var elapsed = 0.0
var refresh = 1.0
var fps_label = null

# Performance monitor numeric constants:
# TIME_FPS = 0
# TIME_PROCESS = 1
# TIME_FIXED_PROCESS = 2
# MEMORY_STATIC = 3
# MEMORY_DYNAMIC = 4
# MEMORY_STATIC_MAX = 5
# MEMORY_DYNAMIC_MAX = 6
# MEMORY_MESSAGE_BUFFER_MAX = 7
# OBJECT_COUNT = 8
# OBJECT_RESOURCE_COUNT = 9
# OBJECT_NODE_COUNT = 10
# RENDER_OBJECTS_IN_FRAME = 11
# RENDER_VERTICES_IN_FRAME = 12
# RENDER_MATERIAL_CHANGES_IN_FRAME = 13
# RENDER_SHADER_CHANGES_IN_FRAME = 14
# RENDER_SURFACE_CHANGES_IN_FRAME = 15
# RENDER_DRAW_CALLS_IN_FRAME = 16
# RENDER_USAGE_VIDEO_MEM_TOTAL = 20
# RENDER_VIDEO_MEM_USED = 17
# RENDER_TEXTURE_MEM_USED = 18
# RENDER_VERTEX_MEM_USED = 19
# PHYSICS_2D_ACTIVE_OBJECTS = 21
# PHYSICS_2D_COLLISION_PAIRS = 22
# PHYSICS_2D_ISLAND_COUNT = 23
# PHYSICS_3D_ACTIVE_OBJECTS = 24
# PHYSICS_3D_COLLISION_PAIRS = 25
# PHYSICS_3D_ISLAND_COUNT = 26
# MONITOR_MAX = 27

func _ready():
	set_process(true)
	fps_label = get_node("Label_FPS")

func _process(delta):
	elapsed += delta

	if elapsed > refresh:
		fps_label.set_text("FPS: " + str(Performance.get_monitor(0)) \
				+ "\nDelta: " + str(Performance.get_monitor(1)) \
				+ "\nFixed Delta: " + str(Performance.get_monitor(2)) \
				+ "\nStatic Memory: " + str(Performance.get_monitor(3)) \
				+ "\nDynamic Memory: " + str(Performance.get_monitor(4)) \
				+ "\nMax Static Memory: " + str(Performance.get_monitor(5)) \
				+ "\nMax Dynamic Memory: " + str(Performance.get_monitor(6)) \
				+ "\nMax Message Buffer Memory: " + str(Performance.get_monitor(7)) \
				+ "\nObject Count: " + str(Performance.get_monitor(8)) \
				+ "\nObject Resource Count: " + str(Performance.get_monitor(9)) \
				+ "\nObject Node Count: " + str(Performance.get_monitor(10)) \
				+ "\nRender Object in Frame: " + str(Performance.get_monitor(11)) \
				+ "\nRender Vertices in Frame: " + str(Performance.get_monitor(12)) \
				+ "\nRender Material Changes in Frame: " + str(Performance.get_monitor(13)) \
				+ "\nRender Shader Changes in Frame: " + str(Performance.get_monitor(14)) \
				+ "\nRender Surface Changes in Frame: " + str(Performance.get_monitor(15)) \
				+ "\nRender Draw Calls in Frame: " + str(Performance.get_monitor(16)) \
				+ "\nRender Video Memory Used: " + str(Performance.get_monitor(17)) \
				+ "\nRender Texture Memory Used: " + str(Performance.get_monitor(18)) \
				+ "\nRender Vertex Memory Used: " + str(Performance.get_monitor(19)) \
				+ "\nRender Video Memory Total Usage: " + str(Performance.get_monitor(20)) \
				+ "\nPhysics2D Active Objects: " + str(Performance.get_monitor(21)) \
				+ "\nPhysics2D Collision Pairs: " + str(Performance.get_monitor(22)) \
				+ "\nPhysics2D Island Count: " + str(Performance.get_monitor(23)) \
				+ "\nPhysics3D Active Objects: " + str(Performance.get_monitor(24)) \
				+ "\nPhysics3D Collision Pairs: " + str(Performance.get_monitor(25)) \
				+ "\nPhysics3D Island Count: " + str(Performance.get_monitor(26)) \
				+ "\nMax Monitor: " + str(Performance.get_monitor(27)))
		elapsed = 0

func _on_SpinBox_value_changed( value ):
	fps_label.set_scale(Vector2(value, value))

func _on_SpinBoxRefresh_value_changed( value ):
	refresh = value
